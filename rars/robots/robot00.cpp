/**
 * Vorlage für den Robot im Software-Projekt
 *
 * @author    Ingo Haschler <lehre@ingohaschler.de>
 * @version   et12
 */

//--------------------------------------------------------------------------
//                           I N C L U D E
//--------------------------------------------------------------------------

#include "car.h"
//--------------------------------------------------------------------------
//                           D E F I N E S
//--------------------------------------------------------------------------

// parameters to tinker with:

const double CORN_MYU   =  .99;                         // Reibungskoeffizient
const double DIST_FROM_INSIDE = 12.0;                   // Abstand vom inneren der Kurve bis zur gewünschten Position
const double DECELERATION = 32;                         // entspricht der negativen Beschleunigung (Verzögerung) beim Bremsen
const double STEER_GAIN = 1.0;                          // Lenkungsverstärker in abh. von Abstand zur Ideallinie
const double STEER_DAMP =.5;                            // Lenkungsabschwächer wenn Auto driftet
const double CURVE_TO_SHORT = 0.4;                     // Ab welcher Kurvenlänge die Kurve einen Bremspunkt braucht (2.14 = 360°)

//--------------------------------------------------------------------------
//                           Class Robot00
//--------------------------------------------------------------------------

class Robot00 : public Driver
{
public:
    // Konstruktor
    Robot00()
    {
        // Der Name des Robots
        m_sName = "Robot00";
        // Namen der Autoren
        m_sAuthor = "";
        // Hier die vorgegebenen Farben eintragen
        m_iNoseColor = oBLUE;
        m_iTailColor = oBLUE;
        m_sBitmapName2D = "car_blue_blue";
        // Für alle Gruppen gleich
        m_sModel3D = "futura";
    }

   double corn_spd(double radius)       // returns maximum cornering speed, fps
  {
    double rad;                        // absolute value of the radius
    rad = radius<0 ? -radius : radius; // make radius positive
    return sqrt(rad * 32.2 * CORN_MYU);// compute the speed
  }

  con_vec drive(situation& s)
    {
        con_vec result = CON_VEC_EMPTY;
        double vc=0;
        double alpha=0;
        double TRACK_WIDTH = s.to_lft+s.to_rgt;

        //es fehlt:
        //Kollisionsvermeidung
        //Steckenbleiben vermeiden
        //Boxenstopp
        //Ideallinie
        //ganz kurze KUrven übergehen

        vc=MAX_SEGMENT_SPEED(s.cur_rad);
        if (BRAKEPOINT_REACHED(s))
            vc=0.95*s.v;
        //vc=s.nex_len;

        alpha = STEER_GAIN * (s.to_lft - IDEAL_LANE(s))/TRACK_WIDTH - STEER_DAMP * s.vn/s.v;

        result.alpha =alpha;
        result.vc=vc;
        return result;
    }

    //berechnet die Idealline für jeden Punkt auf der Strecke
    double IDEAL_LANE(situation& s)
    {
        //Coming soon
        return .5*(s.to_lft+s.to_rgt);
    }

    //Berechnet ob für eine der nächsten drei Kurven gebremst werden muss
    bool BRAKEPOINT_REACHED(situation& s)
    {
        //bremspunkt für nex Segment
        if (s.nex_len>CURVE_TO_SHORT) //Hier evtl eine anspruchsvollere Konstruktion..
            if (0<(-DIST_TO_NEXT_SEGMENT(s)+s.v*((s.v-MAX_SEGMENT_SPEED(s.nex_rad))/DECELERATION)
                    -DECELERATION*0.5*((s.v-MAX_SEGMENT_SPEED(s.nex_rad))/DECELERATION)*((s.v-MAX_SEGMENT_SPEED(s.nex_rad))/DECELERATION)))
                return true;
        //bremspunkt für after Segment
        if (s.after_len>CURVE_TO_SHORT)
            if (0<(-DIST_TO_AFTER_SEGMENT(s)+s.v*((s.v-MAX_SEGMENT_SPEED(s.after_rad))/DECELERATION)
                    -DECELERATION*0.5*((s.v-MAX_SEGMENT_SPEED(s.after_rad))/DECELERATION)*((s.v-MAX_SEGMENT_SPEED(s.after_rad))/DECELERATION)))
                return true;
        //Bremspunkt für aftaft Segment
        if (s.aftaft_len>CURVE_TO_SHORT)
            if (0<(-DIST_TO_AFTAFT_SEGMENT(s)+s.v*((s.v-MAX_SEGMENT_SPEED(s.aftaft_rad))/DECELERATION)
                    -DECELERATION*0.5*((s.v-MAX_SEGMENT_SPEED(s.aftaft_rad))/DECELERATION)*((s.v-MAX_SEGMENT_SPEED(s.aftaft_rad))/DECELERATION)))
                return true;

        return false;
    }

    //bestimmt die maximal fahrbare Geschwindigkeit auf einem Segment
    double MAX_SEGMENT_SPEED(double Radius)
    {
        //erzeugt einen positiven Radius
        if (Radius<0)
            Radius=-Radius;
        //falls es eine Gerade ist, gebe vollgas
        if (Radius == 0)
            return 300;
        //..andernfalls Kurvengeschwindigkeit berechnen
        return sqrt((Radius+DIST_FROM_INSIDE)*32.2*CORN_MYU);
    }

    //Berechnet die Länge einer Kurve
    double CURVE_LENGTH(double Radius, double Winkel)
    {
        if (Radius<0)
            Radius=-Radius;
        return 2*3.141*(Radius+DIST_FROM_INSIDE)*(Winkel/4.28);
    }

    //Berechnet die Strecke bis zum direkt folgendem Segment
    double DIST_TO_NEXT_SEGMENT(situation& s)
    {
        if (s.cur_rad==0)
            return s.to_end;
        else
            return CURVE_LENGTH(s.cur_rad,s.to_end);
    }

    //Berechnet die Strecke bis zu dem Segment hinter dem next Segment
    double DIST_TO_AFTER_SEGMENT(situation& s)
    {
        //berechnet die Länge des nächsten Segmentes und gibt das Gesamtergebnis aus
        if (s.nex_rad==0)
            return DIST_TO_NEXT_SEGMENT(s)+ s.nex_len;
        else
            return DIST_TO_NEXT_SEGMENT(s)+ CURVE_LENGTH(s.nex_rad,s.nex_len);
    }


    //Berechnet die Reststrecke bis zum AFTAFT Segment
    double DIST_TO_AFTAFT_SEGMENT(situation& s)
    {
        if (s.after_rad==0)
            return DIST_TO_AFTER_SEGMENT(s) + s.after_len;
        else
            return DIST_TO_AFTER_SEGMENT(s) + CURVE_LENGTH(s.after_rad,s.after_len);
    }
};


/**
 * Diese Methode darf nicht verändert werden.
 * Sie wird vom Framework aufgerufen, um den Robot zu erzeugen.
 * Der Name leitet sich (wie der Klassenname) von der Gruppenbezeichnung ab.
 */
Driver * getRobot00Instance()
{
    return new Robot00();
}
